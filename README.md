# ssb-server running on a raspberry pi

A Docker container to experiment running a scuttlebut _pub_ server in a classroom environment running on a raspberry pi.

[More documentation on running the pub here](https://ssbc.github.io/docs/scuttlebot/howto-setup-a-pub.html).

@zr+hO8roag+e7krm9aaIDS7Be+AZnFT6AkvbL/SpP2w=.ed25519